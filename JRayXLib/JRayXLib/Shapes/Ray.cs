namespace JRayXLib.Shapes
{
    public class Ray
    {
        public Vect3 Origin { get; set; }
        public Vect3 Direction { get; set; }
    }
}