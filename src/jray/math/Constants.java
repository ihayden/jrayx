package jray.math;

public class Constants {
    public static final double EPS = 1e-9;
    public static double MIN_DISTANCE = EPS;
}
